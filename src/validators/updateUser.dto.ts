import { IsString, IsNotEmpty, Length, IsDate } from 'class-validator';

export class UpdateUserDto {
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  lastName: string;

  @Length(4, 32)
  @IsString()
  @IsNotEmpty()
  nickname: string;

  @IsDate()
  birthday: Date;
}
