import { ClientProxy } from '@nestjs/microservices';
import { anything, deepEqual, mock, objectContaining, when } from 'ts-mockito';
import { secondValidToken, invalidToken } from '../consts/tokens.consts';
import { activeTestUser, inactiveTestUser } from '../consts/users.consts';
import { of, throwError } from 'rxjs';

export const mockedAuthClient: ClientProxy = mock(ClientProxy);

when(
  mockedAuthClient.send(objectContaining({ cmd: 'authenticate' }), anything())
).thenReturn(of(activeTestUser));

when(
  mockedAuthClient.send(
    objectContaining({ cmd: 'authenticate' }),
    secondValidToken
  )
).thenReturn(of(inactiveTestUser));

when(
  mockedAuthClient.send(objectContaining({ cmd: 'authenticate' }), invalidToken)
).thenReturn(throwError({ message: 'Invalid token' }));

when(
  mockedAuthClient.send(
    objectContaining({ cmd: 'deleteTokens' }),
    deepEqual([inactiveTestUser.uuid])
  )
).thenReturn(of(null));
