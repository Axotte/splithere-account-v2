import { User } from '../../database/models/user.model';

export const inactiveTestUser: User = {
  uuid: '3553000c-fad1-44a8-9368-c9cb5838ad42',
  email: 'inactivaUser@test.com',
} as User;

export const activeTestUser: User = {
  uuid: '906dd1db-22d4-4320-b683-175fc204c5bb',
  email: 'inactivaUser@test.com',
  firstName: 'test',
  lastName: 'test',
  nickname: 'test123',
} as User;

export const testFriend: User = {
  uuid: 'f7063a3e-88a7-41e7-894e-c1bb1ac65643',
  email: 'inactivaUser@test.com',
  firstName: 'friend',
  lastName: 'friend',
  nickname: 'friend123',
} as User;
