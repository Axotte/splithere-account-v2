import {
  Friendship,
  FriendshipStatus,
} from '../../database/models/friendship.model';
import { activeTestUser, inactiveTestUser, testFriend } from './users.consts';

export const testFriendship: Friendship = {
  recieverUuid: testFriend.uuid,
  senderUuid: activeTestUser.uuid,
  sender: activeTestUser,
  reciever: testFriend,
  status: FriendshipStatus.ACCEPTED,
} as Friendship;

export const testFriendshipRequest: Friendship = {
  recieverUuid: activeTestUser.uuid,
  senderUuid: inactiveTestUser.uuid,
  sender: inactiveTestUser,
  reciever: activeTestUser,
  status: FriendshipStatus.REQUESTED,
} as Friendship;
