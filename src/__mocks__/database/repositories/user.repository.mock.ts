import { RpcException } from '@nestjs/microservices';
import { anything, mock, objectContaining, when } from 'ts-mockito';
import { IsNull } from 'typeorm';
import { UsersRepository } from '../../../database/repositories/users.repository';
import { invalidToken } from '../../consts/tokens.consts';
import {
  activeTestUser,
  inactiveTestUser,
  testFriend,
} from '../../consts/users.consts';

export const mockedUsersRepository = mock(UsersRepository);

when(mockedUsersRepository.save(anything())).thenResolve(activeTestUser);

when(
  mockedUsersRepository.save(objectContaining({ email: 'existing' }))
).thenReject(new RpcException('User already exist'));

when(mockedUsersRepository.delete(anything())).thenResolve();

when(
  mockedUsersRepository.findOne(
    objectContaining({
      where: { uuid: activeTestUser.uuid },
    })
  )
).thenResolve(activeTestUser);

when(mockedUsersRepository.findOne(testFriend.uuid, anything())).thenResolve(
  testFriend
);

when(
  mockedUsersRepository.findOne(activeTestUser.uuid, anything())
).thenResolve(activeTestUser);

when(
  mockedUsersRepository.findOne(inactiveTestUser.uuid, anything())
).thenResolve(inactiveTestUser);

when(
  mockedUsersRepository.findOne(
    objectContaining({
      where: { uuid: invalidToken },
    })
  )
).thenResolve(undefined);

when(
  mockedUsersRepository.find(
    objectContaining({
      select: ['uuid'],
      where: {
        firstName: IsNull(),
        lastName: IsNull(),
        nickname: IsNull(),
        createdAt: anything(),
      },
    })
  )
).thenResolve([inactiveTestUser]);

when(mockedUsersRepository.getFriendsByUuid(activeTestUser.uuid)).thenResolve([
  testFriend,
]);

when(
  mockedUsersRepository.getFriendshipRequestByUuid(activeTestUser.uuid)
).thenResolve([inactiveTestUser]);
