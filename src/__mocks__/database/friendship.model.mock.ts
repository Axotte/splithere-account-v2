import { mock, when, anything, objectContaining, deepEqual } from 'ts-mockito';
import { In, Repository } from 'typeorm';
import { Friendship } from '../../database/models/friendship.model';
import {
  testFriendship,
  testFriendshipRequest,
} from '../consts/friendship.consts';
import {
  testFriend,
  activeTestUser,
  inactiveTestUser,
} from '../consts/users.consts';

export const mockedFriendshipsRepository: Repository<Friendship> = mock(
  Repository
);

when(
  mockedFriendshipsRepository.findOne(
    deepEqual({
      where: {
        recieverUuid: anything(),
        senderUuid: anything(),
      },
    })
  )
).thenResolve(null);

when(
  mockedFriendshipsRepository.findOne(
    deepEqual({
      where: {
        recieverUuid: In([testFriend.uuid, activeTestUser.uuid]),
        senderUuid: In([testFriend.uuid, activeTestUser.uuid]),
      },
    })
  )
).thenResolve(testFriendship);

when(
  mockedFriendshipsRepository.findOne(
    deepEqual({
      where: objectContaining({
        senderUuid: inactiveTestUser.uuid,
        recieverUuid: activeTestUser.uuid,
      }),
    })
  )
).thenResolve(testFriendshipRequest);

when(
  mockedFriendshipsRepository.findOne(
    deepEqual({
      where: objectContaining({
        senderUuid: testFriend.uuid,
        recieverUuid: activeTestUser.uuid,
      }),
    })
  )
).thenResolve(testFriendship);

when(mockedFriendshipsRepository.save(anything())).thenResolve();

when(mockedFriendshipsRepository.remove(anything())).thenResolve();
