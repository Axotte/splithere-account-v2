import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from '../services/users.service';
import { AccountMicroserviceController } from '../microservice/accountMicroservice.controller';
import { UserController } from './controllers/users.controller';
import { UsersSheduler } from '../schedulers/users.scheruler';
import { MicroserviceClientsModule } from '../microservice/microserviceClients.module';
import { FriendshipsController } from './controllers/friendships.controller';
import { FriendshipsService } from '../services/friendship.service';
import { Friendship } from '../database/models/friendship.model';
import { UsersRepository } from '../database/repositories/users.repository';

@Module({
  providers: [UsersService, UsersSheduler, FriendshipsService],
  controllers: [
    AccountMicroserviceController,
    UserController,
    FriendshipsController,
  ],
  imports: [
    TypeOrmModule.forFeature([Friendship, UsersRepository]),
    MicroserviceClientsModule,
  ],
})
export class AccountModule {}
