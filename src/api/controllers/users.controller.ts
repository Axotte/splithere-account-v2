import { Controller, Get, Body, Patch } from '@nestjs/common';
import { CurrentUser } from '../decorators/currentUser.decorator';
import { UpdateUserDto } from '../../validators/updateUser.dto';
import { UsersService } from '../../services/users.service';
import { User } from '../../database/models/user.model';

@Controller('user')
export class UserController {
  constructor(private readonly usersService: UsersService) {}

  @Get('current')
  getCurrentUser(@CurrentUser() currentUser: User): User {
    return currentUser;
  }

  @Patch('current')
  updateCurrentUser(
    @Body() updateUserDto: UpdateUserDto,
    @CurrentUser() currentUser: User
  ): Promise<void> {
    return this.usersService.updateUser(currentUser, updateUserDto);
  }
}
