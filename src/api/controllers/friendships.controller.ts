import { Controller, Get, Param, ParseUUIDPipe, Post } from '@nestjs/common';
import { User } from '../../database/models/user.model';
import { FriendshipsService } from '../../services/friendship.service';
import { CurrentUser } from '../decorators/currentUser.decorator';

@Controller('friends')
export class FriendshipsController {
  constructor(private readonly friendshipsService: FriendshipsService) {}

  @Get()
  getFiends(@CurrentUser() currentUser: User): Promise<User[]> {
    return this.friendshipsService.getFriends(currentUser);
  }

  @Get('/requests')
  getFriendsipRequests(@CurrentUser() currentUser: User): Promise<User[]> {
    return this.friendshipsService.getFriendshipRequests(currentUser);
  }

  @Post(':uuid/request')
  requestFriendship(
    @Param('uuid', ParseUUIDPipe) uuid: string,
    @CurrentUser() currentUser: User
  ): Promise<void> {
    return this.friendshipsService.requestFriendship(uuid, currentUser);
  }

  @Post(':uuid/accept')
  acceptFriendshipRequest(
    @Param('uuid', ParseUUIDPipe) uuid: string,
    @CurrentUser() currentUser: User
  ): Promise<void> {
    return this.friendshipsService.acceptFriendship(uuid, currentUser);
  }

  @Post(':uuid/deny')
  denyFriendshipRequest(
    @Param('uuid', ParseUUIDPipe) uuid: string,
    @CurrentUser() currentUser: User
  ): Promise<void> {
    return this.friendshipsService.denyFriendship(uuid, currentUser);
  }
}
