import { Exclude } from 'class-transformer';
import {
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  CreateDateColumn,
  Column,
  OneToMany,
  Index,
  JoinColumn,
} from 'typeorm';
import { Friendship } from './friendship.model';

@Entity()
export class User {
  @Index()
  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @Column({ nullable: true })
  firstName: string;

  @Column({ nullable: true })
  lastName: string;

  @Column({ nullable: true })
  nickname: string;

  @Column({ unique: true })
  email: string;

  @Column({ nullable: true })
  avatarUrl: string;

  @Column('date', { nullable: true })
  birthday: Date;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @OneToMany(() => Friendship, (friendship) => friendship.sender, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'recieverUuid' })
  friendshipsSent: Friendship[];

  @OneToMany(() => Friendship, (friendship) => friendship.reciever, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'recieverUuid' })
  friendshipsRecieved: Friendship[];

  getFriends(): User[] {
    return [
      ...this.friendshipsRecieved.map((friendships) => friendships.sender),
      ...this.friendshipsSent.map((friendships) => friendships.reciever),
    ];
  }
}
