import { Entity, Column, ManyToOne, PrimaryColumn, JoinColumn } from 'typeorm';
import { User } from './user.model';

export enum FriendshipStatus {
  REQUESTED = 'REQUESTED',
  ACCEPTED = 'ACCEPTED',
}

@Entity()
export class Friendship {
  @PrimaryColumn()
  senderUuid: string;

  @PrimaryColumn()
  recieverUuid: string;

  @ManyToOne(() => User, (user) => user.friendshipsSent)
  @JoinColumn({ name: 'senderUuid' })
  sender: User;

  @ManyToOne(() => User, (user) => user.friendshipsRecieved)
  @JoinColumn({ name: 'recieverUuid' })
  reciever: User;

  @Column({
    type: 'simple-enum',
    enum: FriendshipStatus,
    default: FriendshipStatus.REQUESTED,
  })
  status: FriendshipStatus;
}
