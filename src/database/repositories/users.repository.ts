import { EntityRepository, Repository } from 'typeorm';
import { FriendshipStatus } from '../models/friendship.model';
import { User } from '../models/user.model';

@EntityRepository(User)
export class UsersRepository extends Repository<User> {
  async getFriendshipRequestByUuid(uuid: string): Promise<User[]> {
    const userWithFriendshipRequests = await this.createQueryBuilder('user')
      .leftJoinAndSelect(
        'user.friendshipsRecieved',
        'friendshipsRecieved',
        'friendshipsRecieved.status = :status',
        {
          status: FriendshipStatus.REQUESTED,
        }
      )
      .leftJoinAndSelect('friendshipsRecieved.sender', 'sender')
      .where('user.uuid = :uuid', { uuid })
      .getOne();

    return userWithFriendshipRequests.friendshipsRecieved.map(
      (friendships) => friendships.sender
    );
  }

  async getFriendsByUuid(uuid: string): Promise<User[]> {
    const userWithFriends = await this.createQueryBuilder('user')
      .leftJoinAndSelect(
        'user.friendshipsRecieved',
        'friendshipsRecieved',
        'friendshipsRecieved.status = :status',
        {
          status: FriendshipStatus.ACCEPTED,
        }
      )
      .leftJoinAndSelect(
        'friendshipsRecieved.sender',
        'sender',
        'sender.uuid <> :uuid',
        {
          uuid,
        }
      )
      .leftJoinAndSelect(
        'user.friendshipsSent',
        'friendshipsSent',
        'friendshipsSent.status = :status',
        {
          status: FriendshipStatus.ACCEPTED,
        }
      )
      .leftJoinAndSelect(
        'friendshipsSent.reciever',
        'reciever',
        'reciever.uuid <> :uuid',
        {
          uuid,
        }
      )
      .where('user.uuid = :uuid', { uuid })
      .getOne();

    return userWithFriends.getFriends();
  }
}
