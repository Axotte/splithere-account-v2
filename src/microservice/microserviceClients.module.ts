import { Module } from '@nestjs/common';
import { ClientProxyFactory, ClientOptions } from '@nestjs/microservices';
import { ConfigService, ConfigModule } from '@nestjs/config';

@Module({
  providers: [
    {
      provide: 'AUTH_MICROSERVICE_CLIENT',
      useFactory: (configService: ConfigService) => {
        const authMicroserviceOptions = configService.get<ClientOptions>(
          'authMicroserviceOptions'
        );
        return ClientProxyFactory.create(authMicroserviceOptions);
      },
      inject: [ConfigService],
    },
  ],
  imports: [ConfigModule],
  exports: ['AUTH_MICROSERVICE_CLIENT'],
})
export class MicroserviceClientsModule {}
