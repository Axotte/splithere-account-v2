import { Controller } from '@nestjs/common';
import { MessagePattern, Payload, RpcException } from '@nestjs/microservices';
import { User } from '../database/models/user.model';
import { UsersService } from '../services/users.service';

@Controller()
export class AccountMicroserviceController {
  constructor(private readonly accountService: UsersService) {}

  @MessagePattern({ cmd: 'createUser' })
  createUser(@Payload() email: string): Promise<User> {
    return this.accountService.createUser(email);
  }

  @MessagePattern({ cmd: 'getUser' })
  getUser(@Payload() uuid: string): Promise<User> {
    return this.accountService.getUserByUuid(uuid);
  }

  @MessagePattern({ cmd: 'getMultipleUsersByUuids' })
  getMultipleUsersByUuids(@Payload() uuids: string[]): Promise<User[]> {
    return this.accountService.getUsersByUuids(uuids);
  }
}
