import { INestApplication, ValidationPipe } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { agent } from 'supertest';
import { instance } from 'ts-mockito';
import { AccountModule } from '../../../api/account.module';
import { AuthGuard } from '../../../api/guards/auth.guard';
import { Friendship } from '../../../database/models/friendship.model';
import { UsersRepository } from '../../../database/repositories/users.repository';
import { validToken } from '../../../__mocks__/consts/tokens.consts';
import {
  inactiveTestUser,
  testFriend,
} from '../../../__mocks__/consts/users.consts';
import { mockedFriendshipsRepository } from '../../../__mocks__/database/friendship.model.mock';
import { mockedUsersRepository } from '../../../__mocks__/database/repositories/user.repository.mock';
import { mockedAuthClient } from '../../../__mocks__/microservice/auth.mock';

describe('Friendships', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const refModule = await Test.createTestingModule({
      providers: [
        {
          provide: APP_GUARD,
          useClass: AuthGuard,
        },
        {
          provide: 'AUTH_MICROSERVICE_CLIENT',
          useValue: instance(mockedAuthClient),
        },
      ],
      imports: [AccountModule],
    })
      .overrideProvider('AUTH_MICROSERVICE_CLIENT')
      .useValue(instance(mockedAuthClient))
      .overrideProvider(getRepositoryToken(Friendship))
      .useValue(instance(mockedFriendshipsRepository))
      .overrideProvider(getRepositoryToken(UsersRepository))
      .useValue(instance(mockedUsersRepository))
      .compile();

    app = refModule.createNestApplication();
    await app.init();
    app.useGlobalPipes(new ValidationPipe());
  });

  test('GET /friends - success', () => {
    return agent(app.getHttpServer())
      .get('/friends')
      .set('Authorization', validToken)
      .expect(200)
      .expect([testFriend]);
  });

  test('GET /friends/requests - success', () => {
    return agent(app.getHttpServer())
      .get('/friends/requests')
      .set('Authorization', validToken)
      .expect(200)
      .expect([inactiveTestUser]);
  });
});
