import { INestApplication, ValidationPipe } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { agent } from 'supertest';
import { instance } from 'ts-mockito';
import { AccountModule } from '../../../api/account.module';
import { AuthGuard } from '../../../api/guards/auth.guard';
import { Friendship } from '../../../database/models/friendship.model';
import { UsersRepository } from '../../../database/repositories/users.repository';
import {
  invalidToken,
  validToken,
} from '../../../__mocks__/consts/tokens.consts';
import { activeTestUser } from '../../../__mocks__/consts/users.consts';
import { mockedFriendshipsRepository } from '../../../__mocks__/database/friendship.model.mock';
import { mockedUsersRepository } from '../../../__mocks__/database/repositories/user.repository.mock';
import { mockedAuthClient } from '../../../__mocks__/microservice/auth.mock';

describe('Users', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const refModule = await Test.createTestingModule({
      providers: [
        {
          provide: APP_GUARD,
          useClass: AuthGuard,
        },
        {
          provide: 'AUTH_MICROSERVICE_CLIENT',
          useValue: instance(mockedAuthClient),
        },
      ],
      imports: [AccountModule],
    })
      .overrideProvider('AUTH_MICROSERVICE_CLIENT')
      .useValue(instance(mockedAuthClient))
      .overrideProvider(getRepositoryToken(Friendship))
      .useValue(instance(mockedFriendshipsRepository))
      .overrideProvider(getRepositoryToken(UsersRepository))
      .useValue(instance(mockedUsersRepository))
      .compile();

    app = refModule.createNestApplication();
    await app.init();
    app.useGlobalPipes(new ValidationPipe());
  });

  test('GET /user/current - success case', () => {
    return agent(app.getHttpServer())
      .get('/user/current')
      .set('Authorization', validToken)
      .expect(200)
      .expect(activeTestUser);
  });

  test('GET /user/current - invalid token', () => {
    return agent(app.getHttpServer())
      .get('/user/current')
      .set('Authorization', invalidToken)
      .expect(401);
  });

  test('GET /user/current - no token', () => {
    return agent(app.getHttpServer()).get('/user/current').expect(401);
  });

  test('POST /user/current', () => {
    return agent(app.getHttpServer())
      .patch('/user/current')
      .set('Authorization', validToken)
      .expect(200);
  });
});
