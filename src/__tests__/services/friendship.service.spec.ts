import {
  BadRequestException,
  ConflictException,
  NotFoundException,
} from '@nestjs/common';
import { instance } from 'ts-mockito';
import { FriendshipsService } from '../../services/friendship.service';
import {
  activeTestUser,
  inactiveTestUser,
  testFriend,
} from '../../__mocks__/consts/users.consts';
import { mockedFriendshipsRepository } from '../../__mocks__/database/friendship.model.mock';
import { mockedUsersRepository } from '../../__mocks__/database/repositories/user.repository.mock';

describe('Users Service', () => {
  const usersService = new FriendshipsService(
    instance(mockedUsersRepository),
    instance(mockedFriendshipsRepository)
  );

  test('request friendship - success', async () => {
    await usersService
      .requestFriendship(inactiveTestUser.uuid, activeTestUser)
      .then((i) => expect(i).toBe(undefined));
  });

  test('request friendship - not existing user', async () => {
    await expect(
      usersService.requestFriendship('invalid uuid', activeTestUser)
    ).rejects.toBeInstanceOf(NotFoundException);
  });

  test('accept friendship - success', async () => {
    await usersService
      .acceptFriendship(inactiveTestUser.uuid, activeTestUser)
      .then((i) => expect(i).toBe(undefined));
  });

  test('accept friendship - not existing user', async () => {
    await expect(
      usersService.acceptFriendship('invalid uuid', activeTestUser)
    ).rejects.toBeInstanceOf(NotFoundException);
  });

  test('accept friendship - not existing request', async () => {
    await expect(
      usersService.acceptFriendship(testFriend.uuid, inactiveTestUser)
    ).rejects.toEqual(new BadRequestException('No request from this user'));
  });

  test('accept friendship - already accepted', async () => {
    await expect(
      usersService.acceptFriendship(testFriend.uuid, activeTestUser)
    ).rejects.toEqual(new BadRequestException('Friendship already accepted'));
  });

  test('deny friendship - success', async () => {
    await usersService
      .denyFriendship(testFriend.uuid, activeTestUser)
      .then((i) => expect(i).toBe(undefined));
  });

  test('deny friendship - not existing user', async () => {
    await expect(
      usersService.denyFriendship('invalid uuid', activeTestUser)
    ).rejects.toBeInstanceOf(NotFoundException);
  });
});
