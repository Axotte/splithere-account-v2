import { instance } from 'ts-mockito';
import { UsersService } from '../../services/users.service';
import { activeTestUser } from '../../__mocks__/consts/users.consts';
import { mockedUsersRepository } from '../../__mocks__/database/repositories/user.repository.mock';
import { mockedAuthClient } from '../../__mocks__/microservice/auth.mock';

describe('Users Service', () => {
  const usersService = new UsersService(
    instance(mockedUsersRepository),
    instance(mockedAuthClient)
  );

  test('delete inactive users', async () => {
    await expect(usersService.deleteInactiveUsers()).resolves.not.toThrow();
  });

  test('create User - success', async () => {
    await expect(
      usersService.createUser(activeTestUser.email)
    ).resolves.toMatchObject(activeTestUser);
  });

  test('create User - user already exist', async () => {
    await expect(usersService.createUser('existing')).rejects.toThrow();
  });

  test('get user by uuid', async () => {
    await expect(
      usersService.getUserByUuid(activeTestUser.uuid)
    ).resolves.toMatchObject(activeTestUser);
  });
});
