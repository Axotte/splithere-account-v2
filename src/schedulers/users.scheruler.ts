import { Injectable, Logger } from '@nestjs/common';
import { UsersService } from '../services/users.service';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class UsersSheduler {
  private readonly logger = new Logger(UsersSheduler.name);

  constructor(private readonly usersService: UsersService) {}

  @Cron(CronExpression.EVERY_DAY_AT_1AM)
  deleteInactiveUsers(): void {
    this.logger.log('"deleteInactiveUsers" called');
    this.usersService.deleteInactiveUsers();
  }
}
