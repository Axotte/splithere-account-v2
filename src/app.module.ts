import { Logger, Module } from '@nestjs/common';
import configuration from './config/config';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { User } from './database/models/user.model';
import { Friendship } from './database/models/friendship.model';
import { AccountModule } from './api/account.module';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './api/guards/auth.guard';
import { ScheduleModule } from '@nestjs/schedule';
import { MicroserviceClientsModule } from './microservice/microserviceClients.module';

@Module({
  imports: [
    AccountModule,
    MicroserviceClientsModule,
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({ load: [configuration] }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          ...configService.get<TypeOrmModuleOptions>('db'),
          entities: [User, Friendship],
        };
      },
    }),
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {}
