import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { User } from '../database/models/user.model';
import {
  Friendship,
  FriendshipStatus,
} from '../database/models/friendship.model';
import { UsersRepository } from '../database/repositories/users.repository';

@Injectable()
export class FriendshipsService {
  constructor(
    private readonly userRepository: UsersRepository,
    @InjectRepository(Friendship)
    private readonly friendshipRepository: Repository<Friendship>
  ) {}

  async requestFriendship(
    recieverUuid: string,
    currentUser: User
  ): Promise<void> {
    const reciever = await this.userRepository.findOne(recieverUuid, {
      select: ['uuid'],
    });

    if (!reciever) throw new NotFoundException('User not found');

    const friendship = await this.friendshipRepository.findOne({
      where: {
        recieverUuid: In([reciever.uuid, currentUser.uuid]),
        senderUuid: In([reciever.uuid, currentUser.uuid]),
      },
    });

    if (friendship) throw new ConflictException('frienship already exist');

    const newRelationship = new Friendship();
    newRelationship.recieverUuid = reciever.uuid;
    newRelationship.senderUuid = currentUser.uuid;

    await this.friendshipRepository.save(newRelationship);
  }

  async acceptFriendship(senderUuid: string, currentUser: User): Promise<void> {
    const sender = await this.userRepository.findOne(senderUuid, {
      select: ['uuid'],
    });

    if (!sender) throw new NotFoundException('User not found');

    const friendship = await this.friendshipRepository.findOne({
      where: {
        recieverUuid: currentUser.uuid,
        senderUuid: sender.uuid,
      },
    });

    if (!friendship) throw new BadRequestException('No request from this user');

    if (friendship.status === FriendshipStatus.ACCEPTED)
      throw new BadRequestException('Friendship already accepted');

    friendship.status = FriendshipStatus.ACCEPTED;

    await this.friendshipRepository.save(friendship);
  }

  async denyFriendship(
    otherUserUuid: string,
    currentUser: User
  ): Promise<void> {
    const otherUser = await this.userRepository.findOne(otherUserUuid, {
      select: ['uuid'],
    });

    if (!otherUser) throw new NotFoundException('User not found');

    const friendship = await this.friendshipRepository.findOne({
      where: {
        recieverUuid: In([otherUser.uuid, currentUser.uuid]),
        senderUuid: In([otherUser.uuid, currentUser.uuid]),
      },
    });

    if (!friendship)
      throw new BadRequestException('Its not friend of current user');

    await this.friendshipRepository.remove(friendship);
  }

  async getFriendshipRequests(user: User): Promise<User[]> {
    return this.userRepository.getFriendshipRequestByUuid(user.uuid);
  }

  async getFriends(user: User): Promise<User[]> {
    return this.userRepository.getFriendsByUuid(user.uuid);
  }
}
