import { Injectable, Inject } from '@nestjs/common';
import { In, IsNull, LessThan } from 'typeorm';
import { User } from '../database/models/user.model';
import { RpcException, ClientProxy } from '@nestjs/microservices';
import { UpdateUserDto } from '../validators/updateUser.dto';
import { UsersRepository } from '../database/repositories/users.repository';

@Injectable()
export class UsersService {
  constructor(
    private readonly usersRepository: UsersRepository,
    @Inject('AUTH_MICROSERVICE_CLIENT') private readonly authClient: ClientProxy
  ) {}

  async createUser(email: string): Promise<User> {
    const newUser = new User();
    newUser.email = email;

    try {
      const createdUser = await this.usersRepository.save(newUser);
      return createdUser;
    } catch (e) {
      throw new RpcException('User already exist');
    }
  }

  async getUserByUuid(uuid: string): Promise<User> {
    return this.usersRepository.findOne({
      where: { uuid },
    });
  }

  async getUsersByUuids(uuids: string[]): Promise<User[]> {
    return this.usersRepository.find({
      where: {
        uuid: In(uuids),
      },
    });
  }

  async updateUser(user: User, updateUserDto: UpdateUserDto): Promise<void> {
    await this.usersRepository.save({ ...user, ...updateUserDto });
  }

  async deleteInactiveUsers(): Promise<void> {
    const now = new Date();
    const users = await this.usersRepository.find({
      select: ['uuid'],
      where: {
        firstName: IsNull(),
        lastName: IsNull(),
        nickname: IsNull(),
        createdAt: LessThan(new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000)),
      },
    });

    if (users?.length) {
      const uuids = users.map((user) => user.uuid);

      await this.authClient.send({ cmd: 'deleteTokens' }, uuids).toPromise();
      await this.usersRepository.delete(uuids);
    }
  }
}
